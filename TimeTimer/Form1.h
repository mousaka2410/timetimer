#pragma once
#include "Form2.h"
#include <vector>
#include "datastructure.hpp"

namespace TimeTimer {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	
	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1()
		{
			InitializeComponent();
			form2 = gcnew Form2();

			array<System::String^>^ text = gcnew array<System::String^>(4);

			System::Xml::XmlDocument ^xmlDoc = gcnew System::Xml::XmlDocument();
			try{
				xmlDoc->Load("data.xml");
				System::Console::WriteLine("Document loaded ok." );
			}
			catch (Exception ^e){
				System::Console::WriteLine("load problem");
				System::Console::WriteLine(e->Message);
			}

			System::IO::TextReader^ reader = gcnew StreamReader("data.txt");

			text[0] = reader->ReadLine();
			text[1] = reader->ReadLine();
			text[2] = reader->ReadLine();
			text[3] = reader->ReadLine();

			reader->Close();

			int nButtons = text->GetLength(0);
			
			for (int i=0; i<nButtons; i++){
				Button^ button = gcnew Button();
				this->flowLayoutPanel1->Controls->Add(button);

				button->Size = System::Drawing::Size(274, 28);
				button->TabIndex = i;
				button->Text = text[i];
				button->UseVisualStyleBackColor = true;
				button->Click += gcnew System::EventHandler(this, &Form1::buttonX_Click);
			}

		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel1;
	protected: 
	public: System::Windows::Forms::NotifyIcon^  notifyIcon1;
	private: 



	private: System::ComponentModel::IContainer^  components;
	private: System::Windows::Forms::Form ^form2;
	
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->notifyIcon1 = (gcnew System::Windows::Forms::NotifyIcon(this->components));
			this->SuspendLayout();
			// 
			// flowLayoutPanel1
			// 
			this->flowLayoutPanel1->Location = System::Drawing::Point(0, 2);
			this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
			this->flowLayoutPanel1->Size = System::Drawing::Size(286, 264);
			this->flowLayoutPanel1->TabIndex = 0;
			// 
			// notifyIcon1
			// 
			this->notifyIcon1->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"notifyIcon1.Icon")));
			this->notifyIcon1->Text = L"TimeTimer";
			this->notifyIcon1->Visible = true;
			this->notifyIcon1->MouseDoubleClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::notifyIcon1_MouseDoubleClick);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 264);
			this->Controls->Add(this->flowLayoutPanel1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->Name = L"Form1";
			this->Text = L"TimeTimer";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void buttonX_Click(System::Object^ sender, System::EventArgs^ e){
				 //this->Visible = false;
				 form2->Text = (cli::safe_cast<Button ^ > (sender))->Text;
				 notifyIcon1->BalloonTipText = form2->Text;
				 notifyIcon1->ShowBalloonTip(0);
				 form2->ShowDialog();
				 this->Visible = true;
			 }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {

		 }
private: System::Void notifyIcon1_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			 this->Activate();
			 form2->Activate();
		 }
};
}

