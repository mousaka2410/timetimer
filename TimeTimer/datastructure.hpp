#ifndef _DATASTRUCTURE_HPP
#define _DATASTRUCTURE_HPP

using namespace System;

[Serializable]
public ref struct Interval{
	long long startTime;
	long long endTime;
	String ^description;
	Interval(long long iStartTime, long long iEndTime) : startTime(iStartTime), endTime(iEndTime) {;}
	Interval() {Interval(0,0);}
	TimeSpan toTimeSpan(void) {return TimeSpan(endTime - startTime);}
};

[Serializable]
public ref struct Activity{
	System::String ^name;
	System::Collections::Generic::List<Interval ^> ^intervals;

	[NonSerialized]
	[Xml::Serialization::XmlIgnore]
	System::TimeSpan ^totalTime;

	Activity(System::String ^iName) : 
		name(iName), intervals(gcnew System::Collections::Generic::List<Interval ^>()){;}
	Activity() {Activity(gcnew System::String(""));}
};


#endif