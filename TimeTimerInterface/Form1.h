#pragma once

#include "../TimeTimer/datastructure.hpp"
#include "EditNameDialog.h"
#include "utils.hpp"

namespace TimeTimerInterface {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();

			//
			//TODO: Add the constructor code here
			//

			//
			//TODO: Should be moved to back-end
			//
			String ^startupPath  = Windows::Forms::Application::StartupPath;
			this->path = System::Environment::GetEnvironmentVariable("HOME") + "\\Documents\\My Dropbox\\TimeTimer\\data.xml";
			IO::TextReader ^reader = gcnew IO::StreamReader(this->path);
			Xml::Serialization::XmlSerializer ^xmlSer =
				gcnew Xml::Serialization::XmlSerializer(Generic::List<Activity ^>::typeid);
			activities = static_cast<Generic::List<Activity ^> ^>(xmlSer->Deserialize(reader));
			reader->Close();

			for (int i=0; i<activities->Count; ++i){
				Activity ^act = activities[i];
				act->totalTime = gcnew System::TimeSpan(0);
				for (int j=0; j<act->intervals->Count; j++){
					act->totalTime = *act->totalTime + act->intervals[j]->toTimeSpan();
				}
			}

			this->listView1->VirtualListSize = activities->Count;
			this->timer1->Start();

			this->editNameDialog = gcnew EditNameDialog();
			this->editNameDialog->Text = L"Add activity";
			currentSelectedItem = -1;
			currentRunningItem = -1;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	// My members
	private: System::Collections::Generic::List<Activity ^> ^activities;
			 System::String ^path;
			 int currentSelectedItem;
			 int currentRunningItem;
			 Interval ^currentInterval;
			 EditNameDialog ^editNameDialog;

	private: System::Windows::Forms::SplitContainer^  splitContainer1;

	private: System::Windows::Forms::ListView^  listView1;
	private: System::Windows::Forms::ColumnHeader^  columnHeader1;

	private: System::Windows::Forms::ColumnHeader^  columnHeader2;
	private: System::Windows::Forms::ColumnHeader^  columnHeader3;
	private: System::Windows::Forms::ColumnHeader^  columnHeader4;
	private: System::Windows::Forms::ToolStrip^  toolStrip1;

	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::ToolStripButton^  toolStripPlayButton;

	private: System::Windows::Forms::Label^  labelTotalTime;
	private: System::Windows::Forms::Label^  labelLabelTotalTime;

	private: System::Windows::Forms::Label^  labelTime;
	private: System::Windows::Forms::Label^  labelLabelTime;
	private: System::Windows::Forms::Label^  labelActivity;

	private: System::Windows::Forms::ImageList^  imageList1;
	private: System::Windows::Forms::ToolStripButton^  toolStripStopButton;
	private: System::Windows::Forms::Timer^  timer2;
	private: System::Windows::Forms::Label^  labelLabelDescription;
	private: System::Windows::Forms::ComboBox^  comboBoxDescription;
	private: System::Windows::Forms::ToolStripButton^  toolStripAddButton;



	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->labelLabelDescription = (gcnew System::Windows::Forms::Label());
			this->comboBoxDescription = (gcnew System::Windows::Forms::ComboBox());
			this->labelTotalTime = (gcnew System::Windows::Forms::Label());
			this->labelLabelTotalTime = (gcnew System::Windows::Forms::Label());
			this->labelTime = (gcnew System::Windows::Forms::Label());
			this->labelLabelTime = (gcnew System::Windows::Forms::Label());
			this->labelActivity = (gcnew System::Windows::Forms::Label());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripPlayButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripStopButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripAddButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->listView1 = (gcnew System::Windows::Forms::ListView());
			this->columnHeader1 = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeader2 = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeader3 = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeader4 = (gcnew System::Windows::Forms::ColumnHeader());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->imageList1 = (gcnew System::Windows::Forms::ImageList(this->components));
			this->timer2 = (gcnew System::Windows::Forms::Timer(this->components));
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->Location = System::Drawing::Point(0, 0);
			this->splitContainer1->Name = L"splitContainer1";
			this->splitContainer1->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->labelLabelDescription);
			this->splitContainer1->Panel1->Controls->Add(this->comboBoxDescription);
			this->splitContainer1->Panel1->Controls->Add(this->labelTotalTime);
			this->splitContainer1->Panel1->Controls->Add(this->labelLabelTotalTime);
			this->splitContainer1->Panel1->Controls->Add(this->labelTime);
			this->splitContainer1->Panel1->Controls->Add(this->labelLabelTime);
			this->splitContainer1->Panel1->Controls->Add(this->labelActivity);
			this->splitContainer1->Panel1->Controls->Add(this->toolStrip1);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->listView1);
			this->splitContainer1->Size = System::Drawing::Size(376, 368);
			this->splitContainer1->SplitterDistance = 198;
			this->splitContainer1->TabIndex = 0;
			// 
			// labelLabelDescription
			// 
			this->labelLabelDescription->AutoSize = true;
			this->labelLabelDescription->Dock = System::Windows::Forms::DockStyle::Top;
			this->labelLabelDescription->Location = System::Drawing::Point(0, 140);
			this->labelLabelDescription->Name = L"labelLabelDescription";
			this->labelLabelDescription->Size = System::Drawing::Size(60, 13);
			this->labelLabelDescription->TabIndex = 7;
			this->labelLabelDescription->Text = L"Description";
			// 
			// comboBoxDescription
			// 
			this->comboBoxDescription->FormattingEnabled = true;
			this->comboBoxDescription->Location = System::Drawing::Point(0, 156);
			this->comboBoxDescription->Name = L"comboBoxDescription";
			this->comboBoxDescription->Size = System::Drawing::Size(121, 21);
			this->comboBoxDescription->TabIndex = 6;
			this->comboBoxDescription->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::comboBoxDescription_SelectedIndexChanged);
			// 
			// labelTotalTime
			// 
			this->labelTotalTime->AutoSize = true;
			this->labelTotalTime->Dock = System::Windows::Forms::DockStyle::Top;
			this->labelTotalTime->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->labelTotalTime->Location = System::Drawing::Point(0, 115);
			this->labelTotalTime->Name = L"labelTotalTime";
			this->labelTotalTime->Size = System::Drawing::Size(90, 25);
			this->labelTotalTime->TabIndex = 5;
			this->labelTotalTime->Text = L"00:00:00";
			// 
			// labelLabelTotalTime
			// 
			this->labelLabelTotalTime->AutoSize = true;
			this->labelLabelTotalTime->Dock = System::Windows::Forms::DockStyle::Top;
			this->labelLabelTotalTime->Location = System::Drawing::Point(0, 102);
			this->labelLabelTotalTime->Name = L"labelLabelTotalTime";
			this->labelLabelTotalTime->Size = System::Drawing::Size(57, 13);
			this->labelLabelTotalTime->TabIndex = 4;
			this->labelLabelTotalTime->Text = L"Total Time";
			// 
			// labelTime
			// 
			this->labelTime->AutoSize = true;
			this->labelTime->Dock = System::Windows::Forms::DockStyle::Top;
			this->labelTime->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->labelTime->Location = System::Drawing::Point(0, 77);
			this->labelTime->Name = L"labelTime";
			this->labelTime->Size = System::Drawing::Size(90, 25);
			this->labelTime->TabIndex = 3;
			this->labelTime->Text = L"00:00:00";
			// 
			// labelLabelTime
			// 
			this->labelLabelTime->AutoSize = true;
			this->labelLabelTime->Dock = System::Windows::Forms::DockStyle::Top;
			this->labelLabelTime->Location = System::Drawing::Point(0, 64);
			this->labelLabelTime->Name = L"labelLabelTime";
			this->labelLabelTime->Size = System::Drawing::Size(30, 13);
			this->labelLabelTime->TabIndex = 2;
			this->labelLabelTime->Text = L"Time";
			// 
			// labelActivity
			// 
			this->labelActivity->AutoSize = true;
			this->labelActivity->Dock = System::Windows::Forms::DockStyle::Top;
			this->labelActivity->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->labelActivity->Location = System::Drawing::Point(0, 39);
			this->labelActivity->Name = L"labelActivity";
			this->labelActivity->Size = System::Drawing::Size(74, 25);
			this->labelActivity->TabIndex = 1;
			this->labelActivity->Text = L"Activity";
			// 
			// toolStrip1
			// 
			this->toolStrip1->ImageScalingSize = System::Drawing::Size(32, 32);
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->toolStripPlayButton, 
				this->toolStripStopButton, this->toolStripAddButton});
			this->toolStrip1->Location = System::Drawing::Point(0, 0);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(376, 39);
			this->toolStrip1->TabIndex = 0;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// toolStripPlayButton
			// 
			this->toolStripPlayButton->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripPlayButton->Enabled = false;
			this->toolStripPlayButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripPlayButton.Image")));
			this->toolStripPlayButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripPlayButton->Name = L"toolStripPlayButton";
			this->toolStripPlayButton->Size = System::Drawing::Size(36, 36);
			this->toolStripPlayButton->Text = L"Start";
			this->toolStripPlayButton->Click += gcnew System::EventHandler(this, &Form1::toolStripButton1_Click);
			// 
			// toolStripStopButton
			// 
			this->toolStripStopButton->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripStopButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripStopButton.Image")));
			this->toolStripStopButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripStopButton->Name = L"toolStripStopButton";
			this->toolStripStopButton->Size = System::Drawing::Size(36, 36);
			this->toolStripStopButton->Text = L"toolStripButton2";
			this->toolStripStopButton->Visible = false;
			this->toolStripStopButton->Click += gcnew System::EventHandler(this, &Form1::toolStripButton2_Click);
			// 
			// toolStripAddButton
			// 
			this->toolStripAddButton->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripAddButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripAddButton.Image")));
			this->toolStripAddButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripAddButton->Name = L"toolStripAddButton";
			this->toolStripAddButton->Size = System::Drawing::Size(36, 36);
			this->toolStripAddButton->Text = L"Add";
			this->toolStripAddButton->Click += gcnew System::EventHandler(this, &Form1::toolStripAddButton_Click);
			// 
			// listView1
			// 
			this->listView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(4) {this->columnHeader1, this->columnHeader2, 
				this->columnHeader3, this->columnHeader4});
			this->listView1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->listView1->FullRowSelect = true;
			this->listView1->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
			this->listView1->Location = System::Drawing::Point(0, 0);
			this->listView1->MultiSelect = false;
			this->listView1->Name = L"listView1";
			this->listView1->Size = System::Drawing::Size(376, 166);
			this->listView1->TabIndex = 0;
			this->listView1->UseCompatibleStateImageBehavior = false;
			this->listView1->View = System::Windows::Forms::View::Details;
			this->listView1->VirtualMode = true;
			this->listView1->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::listView1_SelectedIndexChanged);
			this->listView1->RetrieveVirtualItem += gcnew System::Windows::Forms::RetrieveVirtualItemEventHandler(this, &Form1::listView1_RetrieveVirtualItem);
			// 
			// columnHeader1
			// 
			this->columnHeader1->Text = L"Activity";
			this->columnHeader1->Width = 100;
			// 
			// columnHeader2
			// 
			this->columnHeader2->Text = L"Last Performance";
			this->columnHeader2->Width = 96;
			// 
			// columnHeader3
			// 
			this->columnHeader3->Text = L"Total Time";
			this->columnHeader3->Width = 78;
			// 
			// columnHeader4
			// 
			this->columnHeader4->Text = L"XP";
			this->columnHeader4->Width = 64;
			// 
			// timer1
			// 
			this->timer1->Interval = 60000;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// imageList1
			// 
			this->imageList1->ImageStream = (cli::safe_cast<System::Windows::Forms::ImageListStreamer^  >(resources->GetObject(L"imageList1.ImageStream")));
			this->imageList1->TransparentColor = System::Drawing::Color::Transparent;
			this->imageList1->Images->SetKeyName(0, L"empty.png");
			this->imageList1->Images->SetKeyName(1, L"Actions-player-play-icon-small.png");
			// 
			// timer2
			// 
			this->timer2->Tick += gcnew System::EventHandler(this, &Form1::timer2_Tick);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(376, 368);
			this->Controls->Add(this->splitContainer1);
			this->Name = L"Form1";
			this->Text = L"TimeTimer";
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Form1::Form1_FormClosed);
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel1->PerformLayout();
			this->splitContainer1->Panel2->ResumeLayout(false);
			this->splitContainer1->ResumeLayout(false);
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
private: System::Void listView1_RetrieveVirtualItem(System::Object^  sender, System::Windows::Forms::RetrieveVirtualItemEventArgs^  e) {
			 Activity ^activity = activities[e->ItemIndex];
			 String ^relativeTime = "";
			 int count = activity->intervals->Count;
			 if (0 < count){
				 DateTime ^dt = gcnew DateTime(activity->intervals[count-1]->endTime);
				 relativeTime = toRelativeTime(dt);
			 }
			 e->Item = 
				(gcnew System::Windows::Forms::ListViewItem(
				gcnew cli::array< System::String^  >(4) {
					activity->name,						// Activity name
					relativeTime,						// Relative Time
					TStoString(*activity->totalTime),	// Total Time
					L"" },								// XP 
					-1));
		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 listView1->Refresh();
		 }
private: System::Void listView1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 ListView ^lv = cli::safe_cast<ListView ^>(sender);
			 ListView::SelectedIndexCollection ^selected = lv->SelectedIndices;
			 if (selected->Count > 0){
				 this->setCurrentSelectedItem(selected[0]);
				 this->toolStripPlayButton->Enabled = true;
			 } else {
				 this->toolStripPlayButton->Enabled = false;
			 }
		 }

private: System::Void Form1_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			IO::TextWriter ^writer = gcnew IO::StreamWriter(this->path);
			Xml::Serialization::XmlSerializer ^xmlSer =
				gcnew Xml::Serialization::XmlSerializer(Generic::List<Activity ^>::typeid);
			xmlSer->Serialize(writer, activities);
			writer->Close();
		}

		 // The Start button
private: System::Void toolStripButton1_Click(System::Object^  sender, System::EventArgs^  e) {
			this->toolStripStopButton->Visible = true;
			this->toolStripPlayButton->Visible = false;
			this->comboBoxDescription->Enabled = false;

			currentRunningItem = currentSelectedItem;
			long long ticks = DateTime::Now.Ticks;
			this->currentInterval = gcnew Interval(ticks, ticks);
			if (comboBoxDescription->Text != ""){
				this->currentInterval->description = comboBoxDescription->Text;
			}
			this->timer2->Enabled = true;
			int ix = currentRunningItem;
			activities[ix]->intervals->Add(currentInterval);
			this->listView1->Refresh();
		 }

		 void setCurrentSelectedItem(int ix){
			 this->currentSelectedItem = ix;
			 this->labelActivity->Text = activities[ix]->name;
			 this->labelTotalTime->Text = TStoString(*activities[ix]->totalTime);
			 TimeSpan ts;
			 if (currentSelectedItem == currentRunningItem){
				 ts = currentInterval->toTimeSpan();
			 } else {
				 ts = TimeSpan(0);
			 }
			 this->labelTime->Text = TStoString(ts);
		 }

		 bool addActivity(String ^activityName){
			 if (activityName == "") return false;
			 Activity^ newActivity = gcnew Activity(activityName);
			 newActivity->totalTime = gcnew TimeSpan(0);
			 activities->Add(newActivity);
			 return true;
		 }

		 // The Stop button
private: System::Void toolStripButton2_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->toolStripPlayButton->Visible = true;
			 this->toolStripStopButton->Visible = false;
			 this->timer2->Enabled = false;
			 this->comboBoxDescription->Enabled = true;

			 int ix = currentRunningItem;
			 TimeSpan ^totalTime = activities[ix]->totalTime;
			 *totalTime = totalTime->Add(currentInterval->toTimeSpan());
			 labelTotalTime->Text = TStoString(*totalTime);
			 this->listView1->Refresh();
		 }
private: System::Void timer2_Tick(System::Object^  sender, System::EventArgs^  e) {
			 Interval ^ci = this->currentInterval;
			 ci->endTime = DateTime::Now.Ticks;
			 String ^text = TStoString(ci->toTimeSpan());
			 if (this->labelTime->Text != text){
				 this->labelTime->Text = text;
			 }
		 }


private: System::Void comboBoxDescription_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		 }

private: System::Void toolStripAddButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (editNameDialog->ShowDialog() == Windows::Forms::DialogResult::OK){
				 if (addActivity(editNameDialog->ActivityName)) {
					 listView1->VirtualListSize++;
				 }
			 }
		 }


};
}

