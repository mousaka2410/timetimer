#include "stdafx.h"
#include "utils.hpp"
using namespace System;

String ^TStoString(TimeSpan ts){
	long long frac = ts.Ticks % ts.TicksPerSecond;
	ts = ts - TimeSpan(frac);
	return ts.ToString();
}

String ^toRelativeTime(DateTime ^dt){
	const int SECOND = 1;
	const int MINUTE = 60 * SECOND;
	const int HOUR = 60 * MINUTE;
	const int DAY = 24 * HOUR;
	const int WEEK = 7 * DAY;
	const int MONTH = 30 * DAY;

	System::TimeSpan ^ts = gcnew System::TimeSpan(DateTime::Now.Ticks - dt->Ticks);
	double delta = Math::Abs(ts->TotalSeconds);

	if (delta < 0)
	{
		return "not yet";
	}
	else if (delta < 1){
		return "now";
	}
	else if (delta < 1 * MINUTE){
		return "< a minute ago";
	}
	else if (delta < 2 * MINUTE)
	{
		return "a minute ago";
	}
	else if (delta < 45 * MINUTE)
	{
		return ts->Minutes + " minutes ago";
	}
	else if (delta < 90 * MINUTE)
	{
		return "an hour ago";
	}
	else if (delta < 24 * HOUR)
	{
		return ts->Hours + " hours ago";
	}
	else if (delta < 48 * HOUR)
	{
		return "yesterday";
	}
	else if (delta < 7 * DAY)
	{
		return ts->Days + " days ago";
	}
	else if (delta < 30 * DAY)
	{
		int weeks = Convert::ToInt32(Math::Floor((double)ts->Days / 7));
		return (weeks <= 1) ? "one week ago" : weeks + "weeks ago";
	}
	else if (delta < 12 * MONTH)
	{
		int months = Convert::ToInt32(Math::Floor((double)ts->Days / 30));
		return (months <= 1) ? "one month ago" : months + " months ago";
	}
	else
	{
		int years = Convert::ToInt32(Math::Floor((double)ts->Days / 365));
		return (years <= 1) ? "one year ago" : years + " years ago";
	}
}