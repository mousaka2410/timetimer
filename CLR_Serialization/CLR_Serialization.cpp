// CLR_Serialization.cpp : main project file.

#include "stdafx.h"
#include "../TimeTimer/datastructure.hpp"

using namespace System;

int main(array<System::String ^> ^args)
{
	//--------------------------------------------------------------------------------------
	// Make the data
	System::Collections::Generic::List<Activity ^> ^activities = 
		gcnew System::Collections::Generic::List<Activity ^>();
	activities->Add(gcnew Activity("Project Euler"));
	activities->Add(gcnew Activity("Coding Games"));
	activities->Add(gcnew Activity("Drawing Pixel Art"));
	activities->Add(gcnew Activity("Misc Coding"));
	System::DateTime ^now = System::DateTime::Now;
	Interval ^interval = gcnew Interval(now->Ticks, now->Ticks+1000000000);
	activities[0]->intervals->Add(interval);

	//--------------------------------------------------------------------------------------
	// Generate the XML document
	
	System::Xml::Serialization::XmlSerializer ^ser = 
		gcnew System::Xml::Serialization::XmlSerializer(activities->GetType());
	System::IO::TextWriter ^writer = gcnew System::IO::StreamWriter("data.xml");
	ser->Serialize(writer, activities);
	writer->Close();

	//--------------------------------------------------------------------------------------
	// Read the XML document and print it on screen

	System::Xml::XmlDocument ^xmlDoc = gcnew System::Xml::XmlDocument();
	xmlDoc->Load("data.xml");
	Console::WriteLine(xmlDoc->InnerXml);

	//--------------------------------------------------------------------------------------
	// Deserialize the document and extract some of the data
	System::IO::TextReader ^reader = gcnew System::IO::StreamReader("data.xml");
	System::Collections::Generic::List<Activity ^> ^activities2 = 
		static_cast<System::Collections::Generic::List<Activity^>^>(ser->Deserialize(reader));
	reader->Close();

	for (int i=0; i<activities2->Count; ++i){
		System::Console::WriteLine(activities2[i]->name);
		for (int j=0; j<activities2[i]->intervals->Count; ++j){
			Interval ^interval = activities2[i]->intervals[j];
			System::DateTime ^startTime = gcnew System::DateTime(interval->startTime);
			System::DateTime ^endTime = gcnew System::DateTime(interval->endTime);
			System::Console::WriteLine(L"\t" + startTime->ToLongTimeString());
			System::Console::WriteLine(L"\t" + endTime->ToLongTimeString());
			System::Console::WriteLine(L"\t" + (*endTime-*startTime).ToString());
		}
	}
	
    return 0;
}
